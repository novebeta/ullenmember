<?php
use yii\db\Migration;
/**
 * Class m190724_025728_add_column_access_token_to_user
 */
class m190724_025728_add_column_access_token_to_user extends Migration {
	public function up() {
		$ret = $this->db->createCommand( "SELECT * FROM information_schema.columns WHERE table_schema = DATABASE () and table_name ='user'and column_name ='access_token'" )
		                ->queryOne();
		if ( empty( $ret ) ) {
			$this->addColumn( 'user', 'access_token', $this->string( 255 )
			                                               ->defaultValue( null )
			                                               ->comment( 'token' ) );
		}
	}
	public function down() {
		$this->dropColumn( 'user', 'access_token' );
		return true;
	}
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
	}
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m190724_025728_add_column_access_token_to_user cannot be reverted.\n";
		return false;
	}
}
