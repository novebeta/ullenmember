<?php

use yii\db\Migration;

/**
 * Class m190724_025812_add_column_expire_at_to_user
 */
class m190724_025812_add_column_expire_at_to_user extends Migration
{

	public function up()
	{
		$ret = $this->db->createCommand("SELECT * FROM information_schema.columns WHERE table_schema = DATABASE()  AND table_name = 'user' AND column_name = 'expire_at'")->queryOne();
		if (empty($ret)) {
			$this->addColumn('user','expire_at', $this->integer(11)->defaultValue(NULL)->comment('token expiration time'));
        }
	}

	public function down()
	{
		$this->dropColumn('user', 'expire_at');
		return true;
	}

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190724_025812_add_column_expire_at_to_user cannot be reverted.\n";

        return false;
    }
}
