<?php
namespace app\modules\api\models;
use Yii;
use yii\web\UnauthorizedHttpException;
/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $join_date
 * @property string $name
 * @property string $phone
 * @property string $address
 * @property string $info
 * @property string $tgl_lahir
 * @property string $job
 * @property string $no_member
 * @property string $ktp
 * @property string $access_token token
 * @property string $password
 * @property string $authKey
 * @property int $expire_at token expiration time
 */
class User extends \app\models\User {
	/**
	 * Generate accessToken string
	 * @return string
	 * @throws \yii\base\Exception
	 */
	public function generateAccessToken() {
		$this->access_token = Yii::$app->security->generateRandomString();
		return $this->access_token;
	}

	public static function findIdentityByAccessToken($token, $type = null)
	{
//        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
		$user = static::find()->where(['access_token' => $token, 'status' => self::STATUS_ACTIVE])->one();
		if (!$user) {
			return false;
		}
		if ($user->expire_at < time()) {
			throw new UnauthorizedHttpException('the access - token expired ', -1);
		} else {
			return $user;
		}
	}
}