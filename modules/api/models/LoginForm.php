<?php
namespace app\modules\api\models;
use Yii;
use yii\base\Model;
/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model {
	const EXPIRE_TIME = 604800;
	public $email; //
	public $password;
	public $rememberMe = true;
	/**
	 * @var User $_user
	 */
	private $_user = false;
	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		return [
			// username and password are both required
			[ [ 'email', 'password' ], 'required' ],
			[ [ 'email' ], 'email' ], //
			// rememberMe must be a boolean value
			[ 'rememberMe', 'boolean' ],
			// password is validated by validatePassword()
			[ 'password', 'validatePassword' ],
		];
	}
	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 * @param array $params the additional name-value pairs given in the rule
	 */
	public function validatePassword( $attribute, $params ) {
		if ( ! $this->hasErrors() ) {
			$user = $this->getUser();
			if ( ! $user || ! $user->validatePassword( $this->password ) ) {
				$this->addError( $attribute, 'Incorrect username or password.' );
			}
		}
	}
	/**
	 * Logs in a user using the provided username and password.
	 * @return bool whether the user is logged in successfully
	 * @throws \yii\base\Exception
	 */
	public function login()
	{
		if ($this->validate()) {
			//return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
			if ($this->getUser()) {
				$access_token = $this->_user->generateAccessToken();
				$this->_user->expire_at = time() + static::EXPIRE_TIME;
				$this->_user->save();
				Yii::$app->user->login($this->_user, static::EXPIRE_TIME);
				return $access_token;
			}
		}
		return false;
	}

	/**
	 * Finds user by [[username]]
	 *
	 * @return User|null
	 */
	public function getUser() {
		if ( $this->_user === false ) {
			$this->_user = User::findByEmail( $this->email ); //
		}
		return $this->_user;
	}
}
