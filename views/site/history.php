<?php
/* @var $this yii\web\View */
$this->title = 'History';
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Transaksi Point</h3>
                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>Ref</th>
                        <th>Date</th>
                        <th class="pull-right">Point</th>
                    </tr>
					<?php
                    $i = 1;
					foreach ( $dataProvider->models as $model ) :
						?>
                        <tr>
                            <td><?= $i; ?>.</td>
                            <td><?= $model->ref; ?></td>
                            <td><?= $model->tgl; ?></td>
                            <td class="badge bg-green pull-right"><?= $model->point; ?></td>
                        </tr>
					<?php
                    $i++;
					endforeach;
					?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
