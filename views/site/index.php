<?php
/* @var $this yii\web\View */
$this->title = 'Membership';
use app\models\User;
/** $member User */
$member    = User::getCurrentUser();
$formatter = Yii::$app->formatter;
?>
<div class="row">
    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg"
                     alt="User profile picture">
                <h3 class="profile-username text-center"><?= $member->name; ?></h3>
                <p class="text-muted text-center"><?= $member->job; ?></p>
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Birthdate</b> <a
                                class="pull-right"><?= $formatter->asDate( $member->tgl_lahir, 'long' ); ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Phone</b> <a class="pull-right"><?= $member->phone; ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Email</b> <a class="pull-right"><?= $member->email; ?></a>
                    </li>
                    <li class="list-group-item">
                        <b>Point</b> <a class="pull-right">0</a>
                    </li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- About Me Box -->
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
                <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>
                <hr>
                <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                <p class="text-muted"><?= $member->address; ?></p>
                <hr>
                <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Node.js</span>
                </p>
                <hr>
                <strong><i class="fa fa-file-text-o margin-r-5"></i> Info</strong>
                <p><?= $member->info; ?></p>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
