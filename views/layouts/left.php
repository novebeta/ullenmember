<?php
use app\models\User;
$member    = User::getCurrentUser();
?>
<aside class="main-sidebar">
    <section class="sidebar">
		<?= dmstr\widgets\Menu::widget(
			[
				'options' => [ 'class' => 'sidebar-menu tree', 'data-widget' => 'tree' ],
				'items'   => [
					[ 'label' => 'MAIN NAVIGATION', 'options' => [ 'class' => 'header' ] ],
					[ 'label' => 'Profile', 'icon' => 'file-code-o', 'url' => [ 'site/index' ] ],
					[ 'label' => 'Histori', 'icon' => 'file-code-o', 'url' => [ 'site/history' ] ],
					[ 'label' => 'Kartu', 'icon' => 'file-code-o', 'url' => [ 'site/card' ] ],
					[ 'label' => 'Ganti Passwrd', 'icon' => 'file-code-o', 'url' => [ 'site/pass' ] ],
//					[ 'label' => 'Debug', 'icon' => 'dashboard', 'url' => [ '/debug' ] ],
//					[ 'label' => 'Login', 'url' => [ 'site/login' ], 'visible' => Yii::$app->user->isGuest ],
//					[
//						'label' => 'Some tools',
//						'icon'  => 'share',
//						'url'   => '#',
//						'items' => [
//							[ 'label' => 'Gii', 'icon' => 'file-code-o', 'url' => [ '/gii' ], ],
//							[ 'label' => 'Debug', 'icon' => 'dashboard', 'url' => [ '/debug' ], ],
//							[
//								'label' => 'Level One',
//								'icon'  => 'circle-o',
//								'url'   => '#',
//								'items' => [
//									[ 'label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#', ],
//									[
//										'label' => 'Level Two',
//										'icon'  => 'circle-o',
//										'url'   => '#',
//										'items' => [
//											[ 'label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#', ],
//											[ 'label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#', ],
//										],
//									],
//								],
//							],
//						],
//					],
				],
			]
		) ?>
    </section>
</aside>
