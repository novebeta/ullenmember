<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_point_trans".
 *
 * @property string $member_point_trans_id
 * @property string $ref
 * @property string $tgl
 * @property string $trans_no
 * @property int $trans_tipe
 * @property int $point
 */
class MemberPointTrans extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member_point_trans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_point_trans_id', 'point'], 'required'],
            [['tgl'], 'safe'],
            [['trans_tipe', 'point'], 'integer'],
            [['member_point_trans_id', 'trans_no'], 'string', 'max' => 36],
            [['ref'], 'string', 'max' => 50],
            [['member_point_trans_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'member_point_trans_id' => 'Member Point Trans ID',
            'ref' => 'Ref',
            'tgl' => 'Tgl',
            'trans_no' => 'Trans No',
            'trans_tipe' => 'Trans Tipe',
            'point' => 'Point',
        ];
    }

    /**
     * {@inheritdoc}
     * @return MemberPointTransQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MemberPointTransQuery(get_called_class());
    }
}
