<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MemberPointTrans;

/**
 * MemberPointTransSearch represents the model behind the search form of `app\models\MemberPointTrans`.
 */
class MemberPointTransSearch extends MemberPointTrans
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_point_trans_id', 'ref', 'tgl', 'trans_no'], 'safe'],
            [['trans_tipe', 'point'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MemberPointTrans::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tgl' => $this->tgl,
            'trans_tipe' => $this->trans_tipe,
            'point' => $this->point,
        ]);

        $query->andFilterWhere(['like', 'member_point_trans_id', $this->member_point_trans_id])
            ->andFilterWhere(['like', 'ref', $this->ref])
            ->andFilterWhere(['like', 'trans_no', $this->trans_no]);

        return $dataProvider;
    }
}
