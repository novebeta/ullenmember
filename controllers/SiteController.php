<?php
namespace app\controllers;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\MemberPointTransSearch;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
class SiteController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => [ 'login', 'logout', 'index' ],
				'rules' => [
					[
						'actions' => [ 'login', 'error' ],
						'allow'   => true,
					],
					[
						'actions' => [ 'logout', 'index' ],
						'allow'   => true,
						'roles'   => [ '@' ],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => [ 'post' ],
				],
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex() {
		return $this->render( 'index' );
	}
	/**
	 * Displays Card.
	 *
	 * @return string
	 */
	public function actionCard() {
		return $this->render( 'card' );
	}
	/**
	 * @return string
	 */
	public function actionHistory() {
		$searchModel  = new MemberPointTransSearch();
		$dataProvider = $searchModel->search( Yii::$app->request->queryParams );
		return $this->render( 'history', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}
	/**
	 * Login action.
	 *
	 * @return Response|string
	 */
	public function actionLogin() {
		if ( ! Yii::$app->user->isGuest ) {
			return $this->goHome();
		}
		$model = new LoginForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->login() ) {
			return $this->goBack();
		}
		$model->password = '';
		$this->layout    = '@app/views/layouts/main-login';
		return $this->render( 'login', [
			'model' => $model,
		] );
	}
	/**
	 * Logout action.
	 *
	 * @return Response
	 */
	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}
	/**
	 * Displays contact page.
	 *
	 * @return Response|string
	 */
	public function actionContact() {
		$model = new ContactForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->contact( Yii::$app->params['adminEmail'] ) ) {
			Yii::$app->session->setFlash( 'contactFormSubmitted' );
			return $this->refresh();
		}
		return $this->render( 'contact', [
			'model' => $model,
		] );
	}
	/**
	 * Displays about page.
	 *
	 * @return string
	 */
	public function actionAbout() {
		return $this->render( 'about' );
	}
	public function actionAdmin() {
		$model = User::find()->where( [ 'username' => 'admin' ] )->one();
		if ( empty( $model ) ) {
			$model = new User();
		}
//		$model->username = 'admin';
//		$model->email    = 'admin@devreadwrite.com';
		$model->setPassword( 'kBIcRhTvgYRRc5DdVXvCqmhsfXzXlVDN7Y2QuEx0egj0pZKimOg2zJSi3SeC/7dI7l1eqCrVMZvhS68ErFi7RA' );
		$model->generateAuthKey();
		if ( $model->save() ) {
			echo 'good';
		}
	}
}
